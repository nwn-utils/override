#!/bin/sh

if [ -d "$1" ]; then
	DEST="$1"
elif [ -n "$1" ]; then
	echo "Error; not a directory: $1"
	exit 1
else
	DEST=.
fi

BASEDIR=$(dirname "$0")
find "$BASEDIR" -path "$BASEDIR/.git" -prune \
	-o -path "$BASEDIR/*/*" \
	-a \! -name 'README.md' -exec cp {} "$DEST/" \;

