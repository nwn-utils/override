// Loot Items (and Use Placeables)
//   This differs from the normal Loot script in that it will
//   Interact with (Use) Placeable objects without an inventory,
//   like Gold Piles and such.
// Author: Palswim (2019-07-06)

#include "nw_i0_generic"
#include "nw_i0_henchman"

int DEBUG = FALSE;

void TakeAllGold(object oTaker, object oGiver) {
    AssignCommand(oTaker, TakeGoldFromCreature(GetGold(oGiver), oGiver));
}

object BestLoreInGroup(object oMaster) {
    object oIdentifier = oMaster;
    object oHench = oMaster;
    int skillLoreMax = 0;
    int henchNum = 1;
    do {
        int skillLore = GetSkillRank(SKILL_LORE, oHench);
        if (skillLore > skillLoreMax) {
            oIdentifier = oHench;
            skillLoreMax = skillLore;
        }
    } while (GetIsObjectValid(oHench = GetHenchman(oMaster, henchNum++)));
    return oIdentifier;
}

int IdentifyMaxValue(object oTarget) {
    int skillLore = GetSkillRank(SKILL_LORE, oTarget);
    string gpMax = Get2DAString("SkillVsItemCost", "DeviceCostMax", skillLore); // max value that the target can id
    return ((gpMax == "") ? -1 : StringToInt(gpMax));
}

void IdentifyItemIfPossible(object cMaster, string nameIdentifier, object item, int gpMax) {
    if (GetIdentified(item))
        return;
    SetIdentified(item, TRUE);
    if ((gpMax >= 0) && (GetGoldPieceValue(item) > gpMax))
        SetIdentified(item, FALSE);
    else
        SendMessageToPC(cMaster, (nameIdentifier + " identified " + GetName(item)));
}

void TakeItem(string nameActor, object item, object cMaster, object container, string nameIdentifier, int gpMax) {
    IdentifyItemIfPossible(cMaster, nameIdentifier, item, gpMax);
    ActionTakeItem(item, container);
    if ((GetBaseItemType(item) == BASE_ITEM_GOLD) && (OBJECT_SELF != cMaster))
        // If a henchman takes Gold, it doesn't show in his inventory
        ActionDoCommand(TakeAllGold(cMaster, OBJECT_SELF));
    else if (OBJECT_SELF != cMaster)
        ActionDoCommand(SendMessageToPC(cMaster, (nameActor + " picked up " + GetName(item) + " from " + GetName(container) + ".")));
}

void GetAllItemsFrom(object cActor, string nameActor, object container, object cMaster, string nameIdentifier, int gpMax) {
    if (GetLocked(container)) // Don't search locked containers
        return;
    if (GetIsTrapped(container)) {
        if (GetTrapDetectedBy(container, cActor))
            return; // Don't touch trapped containers if you can see the trap
        AssignCommand(cActor, ActionInteractObject(container));
    } // Only search trapped containers if you can't see the trap
    else {
        // Do we want to Open the Thing to generate any random loot?
        //AssignCommand(cActor, ActionInteractObject(container));
    }

    int hadFirstEncounter = FALSE;

    object item = GetFirstItemInInventory(container);
    while (GetIsObjectValid(item))
    {
        AssignCommand(cActor, TakeItem(nameActor, item, cMaster, container, nameIdentifier, gpMax));
        if (!hadFirstEncounter) {
            //ActionMoveToObject(oThing);
            //ActionPlayAnimation(ANIMATION_LOOPING_GET_MID,1.0,3.0);
            AssignCommand(cActor, ActionDoCommand(AssignCommand(container, PlayAnimation(ANIMATION_PLACEABLE_OPEN))));
            hadFirstEncounter = TRUE;
        }
        item = GetNextItemInInventory(container);
    }

    if (hadFirstEncounter) // Close when the Actor finishes
        AssignCommand(cActor, ActionDoCommand(AssignCommand(container, DelayCommand(0.5, PlayAnimation(ANIMATION_PLACEABLE_CLOSE)))));
}

void LootAllItemsAndReport(object oActor, object oMaster) {
    object oIdentifier = BestLoreInGroup(oMaster);
    string nameActor = GetName(oActor);
    string nameIdentifier = GetName(oIdentifier);
    if (DEBUG)
        SendMessageToPC(oMaster, (nameActor + " retrieving items for " + GetName(oMaster)
            + ", and " + nameIdentifier + " identifying. (OBJECT_SELF: " + GetName(OBJECT_SELF) + ")"));

    int gpMax = IdentifyMaxValue(oIdentifier);
    AssignCommand(oActor, ClearAllActions());
    SetAssociateState(NW_ASC_IS_BUSY, TRUE, oActor);
    location lCenter = GetLocation(oActor);
    object oThing = GetFirstObjectInShape(SHAPE_SPHERE, 30.0, lCenter, FALSE, OBJECT_TYPE_PLACEABLE | OBJECT_TYPE_ITEM);
    while (GetIsObjectValid(oThing))
    {
        if (GetObjectType(oThing) == OBJECT_TYPE_ITEM)
        {
            IdentifyItemIfPossible(oMaster, nameIdentifier, oThing, gpMax);
            AssignCommand(oActor, ActionPickUpItem(oThing));
            if (oActor != oMaster)
                AssignCommand(oActor, ActionDoCommand(SendMessageToPC(oMaster, (nameActor + " picked up " + GetName(oThing) + "."))));
        }
        else if (GetHasInventory(oThing)) // oThing is a placeable
        {
            GetAllItemsFrom(oActor, nameActor, oThing, oMaster, nameIdentifier, gpMax);
        }
        else // oThing is a placeable, but does not have an inventory, like a Gold Pile
        {
            AssignCommand(oActor, ActionInteractObject(oThing));
            if (oActor != oMaster)
                AssignCommand(oActor, ActionDoCommand(TakeAllGold(oMaster, OBJECT_SELF)));
        }
        oThing = GetNextObjectInShape(SHAPE_SPHERE, 30.0, lCenter, FALSE, OBJECT_TYPE_PLACEABLE | OBJECT_TYPE_ITEM);
    }
    AssignCommand(oActor, ActionDoCommand(SetAssociateState(NW_ASC_IS_BUSY, FALSE, oActor)));
    AssignCommand(oActor, ActionDoCommand(SpeakString("Finished looting!")));
}

void main()
{
    object cActor = GetItemActivatedTarget(); // Even this call can return stale data, unfortunately
    switch (GetObjectType(OBJECT_SELF)) {
    case OBJECT_TYPE_CREATURE:
        if (GetObjectType(cActor) != OBJECT_TYPE_CREATURE)
            cActor = OBJECT_SELF;
        break;
    default: // For some Item Activation Scripts, OBJECT_SELF is actually the Module
        if (GetObjectType(cActor) != OBJECT_TYPE_CREATURE)
            cActor = GetItemActivator();

        if (!GetIsObjectValid(cActor)) {
            if (DEBUG)
                SendMessageToPC(GetFirstPC(), "Unsupported Object: " + GetName(OBJECT_SELF));
            return;
        }
        break;
    }

    object cMaster = GetMaster(cActor);
    if (!GetIsObjectValid(cMaster))
        cMaster = cActor;
    LootAllItemsAndReport(cActor, cMaster);
}

