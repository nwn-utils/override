//Contributed by Mike Daneman aka. Mishenka
//Modified: 69MEH69 Added more items to pick up
//Modified: Palswim (2019-04-16): Added hooks to use script with PC and Item Activation as well


#include "nw_i0_generic"
#include "nw_i0_henchman"

int DEBUG = FALSE;

void TakeAllGold(object oTaker, object oGiver) {
    AssignCommand(oTaker, TakeGoldFromCreature(GetGold(oGiver), oGiver));
}

object BestSkillInGroup(object oMaster, int skill) {
    object oIdentifier = oMaster;
    object oHench = oMaster;
    int rkMax = -1;
    int henchNum = 1;
    do {
        int rkSkill = GetSkillRank(skill, oHench);
        if (rkSkill > rkMax) {
            oIdentifier = oHench;
            rkMax = rkSkill;
        }
    } while (GetIsObjectValid(oHench = GetHenchman(oMaster, henchNum++)));
    return oIdentifier;
}

int IdentifyMaxValue(object oTarget) {
    int skillLore = GetSkillRank(SKILL_LORE, oTarget);
    string gpMax = Get2DAString("SkillVsItemCost", "DeviceCostMax", skillLore); // max value that the target can id
    return ((gpMax == "") ? ((skillLore > 0) ? -1 : 0) : StringToInt(gpMax));
}

void IdentifyItemIfPossible(object cMaster, string nameIdentifier, object item, int gpMax) {
    if (GetIdentified(item))
        return;
    SetIdentified(item, TRUE);
    if ((gpMax >= 0) && (GetGoldPieceValue(item) > gpMax))
        SetIdentified(item, FALSE);
    else
        SendMessageToPC(cMaster, (nameIdentifier + " identified " + GetName(item)));
}

void TakeItem(object item, object cMaster, object container, string nameIdentifier, int gpMax) {
    IdentifyItemIfPossible(cMaster, nameIdentifier, item, gpMax);
    ActionTakeItem(item, container);
    if ((GetBaseItemType(item) == BASE_ITEM_GOLD) && (OBJECT_SELF != cMaster))
        // If a henchman takes Gold, it doesn't show in his inventory
        ActionDoCommand(TakeAllGold(cMaster, OBJECT_SELF));
    else if (OBJECT_SELF != cMaster)
        ActionDoCommand(SendMessageToPC(cMaster, (GetName(OBJECT_SELF) + " picked up " + GetName(item) + " from " + GetName(container) + ".")));
}

void GetAllItemsFrom(object cActor
    , object container
    , object cMaster
    , string nameIdentifier, int gpMax
    , object cSpotter  = OBJECT_INVALID
    , object cLockpick = OBJECT_INVALID) {
    if (GetLocked(container)) {
        return; // No auto-unlock for now
        if ((!GetLockKeyRequired(container)) && ((GetSkillRank(SKILL_OPEN_LOCK, cLockpick) + 20) >= GetLockLockDC(container))) {
            AssignCommand(cLockpick, ActionUnlockObject(container));
            // Re-try loot after unlocking the Container
            AssignCommand(cLockpick, ActionDoCommand(GetAllItemsFrom(cActor, container, cMaster, nameIdentifier, gpMax, cSpotter)));
        }
    }

    if (GetIsTrapped(container)) {
        // Should we see if cSpotter can detect? Not right now
        if (GetTrapDetectedBy(container, cActor))
            return; // Don't touch trapped containers if you can see the trap
        AssignCommand(cActor, ActionInteractObject(container));
    } // Only search trapped containers if you can't see the trap
    else {
        // Do we want to Open the Thing to generate any random loot?
        //AssignCommand(cActor, ActionInteractObject(container));
    }

    int hadFirstEncounter = FALSE;

    object item = GetFirstItemInInventory(container);
    while (GetIsObjectValid(item))
    {
        AssignCommand(cActor, TakeItem(item, cMaster, container, nameIdentifier, gpMax));
        if (!hadFirstEncounter) {
            //ActionMoveToObject(oThing);
            //ActionPlayAnimation(ANIMATION_LOOPING_GET_MID,1.0,3.0);
            AssignCommand(cActor, ActionDoCommand(AssignCommand(container, PlayAnimation(ANIMATION_PLACEABLE_OPEN))));
            hadFirstEncounter = TRUE;
        }
        item = GetNextItemInInventory(container);
    }

    if (hadFirstEncounter) // Close when the Actor finishes
        AssignCommand(cActor, ActionDoCommand(AssignCommand(container, DelayCommand(0.5, PlayAnimation(ANIMATION_PLACEABLE_CLOSE)))));
}

void Action_End(object cMaster) {
    SetAssociateState(NW_ASC_IS_BUSY, FALSE);
    //SendMessageToPC(cMaster, (GetName(OBJECT_SELF) + " finished looting"));
    SpeakString("Finished looting.", TALKVOLUME_SHOUT);
}

void LootAllItemsAndReport(object cActor, object cMaster) {
    object cIdentifier = BestSkillInGroup(cMaster, SKILL_LORE);
    object cSpotter = BestSkillInGroup(cMaster, SKILL_SEARCH); // Spotter for Traps
    object cLockpick = BestSkillInGroup(cMaster, SKILL_OPEN_LOCK); // Lock-picker
    string nameActor = GetName(cActor);
    string nameIdentifier = GetName(cIdentifier);
    if (DEBUG)
        SendMessageToPC(cMaster, (nameActor + " retrieving items for " + GetName(cMaster)
            + ", and " + nameIdentifier + " identifying. (OBJECT_SELF: " + GetName(OBJECT_SELF) + ")"));

    int gpMax = IdentifyMaxValue(cIdentifier);
    AssignCommand(cActor, ClearAllActions());
    SetAssociateState(NW_ASC_IS_BUSY, TRUE, cActor);
    location lCenter = GetLocation(cActor);
    object obj = GetFirstObjectInShape(SHAPE_SPHERE, 30.0, lCenter, FALSE, OBJECT_TYPE_PLACEABLE | OBJECT_TYPE_ITEM);
    while (GetIsObjectValid(obj))
    {
        if (GetObjectType(obj) == OBJECT_TYPE_ITEM)
        {
            IdentifyItemIfPossible(cMaster, nameIdentifier, obj, gpMax);
            AssignCommand(cActor, ActionPickUpItem(obj));
            if (cActor != cMaster)
                AssignCommand(cActor, ActionDoCommand(SendMessageToPC(cMaster, (nameActor + " picked up " + GetName(obj) + "."))));
        }
        else if (GetHasInventory(obj)) // oThing is a placeable
        {
            GetAllItemsFrom(cActor, obj, cMaster, nameIdentifier, gpMax, cSpotter, cLockpick);
        }
        obj = GetNextObjectInShape(SHAPE_SPHERE, 30.0, lCenter, FALSE, OBJECT_TYPE_PLACEABLE | OBJECT_TYPE_ITEM);
    }
    AssignCommand(cActor, ActionDoCommand(Action_End(cMaster)));
}

void main()
{
    object cActor = GetItemActivatedTarget(); // Even this call can return stale data, unfortunately
    switch (GetObjectType(OBJECT_SELF)) {
    case OBJECT_TYPE_CREATURE:
        if (GetObjectType(cActor) != OBJECT_TYPE_CREATURE)
            cActor = OBJECT_SELF;
        break;
    default: // For some Item Activation Scripts, OBJECT_SELF is actually the Module
        if (GetObjectType(cActor) != OBJECT_TYPE_CREATURE)
            cActor = GetItemActivator();

        if (!GetIsObjectValid(cActor)) {
            if (DEBUG)
                SendMessageToPC(GetFirstPC(), "Unsupported Object: " + GetName(OBJECT_SELF));
            return;
        }
        break;
    }

    object cMaster = GetMaster(cActor);
    if (!GetIsObjectValid(cMaster))
        cMaster = cActor;
    LootAllItemsAndReport(cActor, cMaster);
}

