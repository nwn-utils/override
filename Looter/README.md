# The Looter

**The Looter** allows a character to take the items from the surrounding area, either directly or from containers.

## Background

Playing through [the Aielund Saga](https://neverwintervault.org/project/nwn1/module/aielund-saga-act-i-nature-abhors-vacuum), I found that my henchmen had the ability to loot the surrounding items on request through a dialogue option. I wanted to use this feature through an Item and generic script, as well as adding automatic Identification of objects, and so created The Looter and its corresponding script.

## Technical details

Invocation of the script directly (`dm_runscript ps_lootitems`) will cause the PC to loot the area. Usage of the Item (`ps_it_looter`) will cause the target of the item's Unique Power to loot the area. To use this item as a player, place these files in your `override` directory, and ensure you are using [Tag-Based Scripting](https://forum.neverwintervault.org/t/adding-tag-based-scripting-to-oc-modules/1236) in your module.

Right now, the Looter only takes static items from a container, without triggering the Open (`OnOpen`) or Death (`OnDeath`) scripts of the container, the most common triggers to generate random treasure. Those scripts still will trigger upon opening or bashing of the container. I did this to avoid accidentally generating sub-par or incorrect loot if a henchman opens the container, rather than the PC, because [sometimes the opener determines the container's (semi-)random treasure](https://forum.neverwintervault.org/t/determine-level-for-random-loot-and-spawns/721). To trigger the `OnOpen` script and generate the treasure, I could un-comment the line underneath the `Do we want to Open the Thing to generate any random loot?` comment. You could use another [Utility](/nwn-utils/override) item, [The Basher](../Basher) in conjunction with The Looter to clear the room completely and automatically (and [I've even created a 2-line script](../Basher/ps_bashloot.ncs) that Bashes, waits 5 seconds, then Loots to do everything in one shot).

Keep in mind that a module designer may have made certain assumptions about the PC acquiring a certain item directly, rather than through a henchman, so this item may break your game if your module triggers plot-critical points on the retrieval of an item and your henchman retieves the item instead of your PC. Most modules shouldn't do this, but you may run into one occasionally.

I built this script from the existing `henchpickemup` script in one of the Aielund modules, which I believe was using the [Henchman Kit by 69MEH69](https://neverwintervault.org/project/nwn1/hakpak/original-hakpak/module-builders-henchman-kit).

## Files
 - `ps_it_looter`: The Looter (Miscellaneous Small item that can target Self or another Creature to compel the target to loot the surrounding items)
 - `ps_lootitems`: The script which the Item uses, but also works for the PC by invocation with `dm_runscript`
 - `ps_lootitems_use`: Additional script for using items before attempting loot (to trigger the `OnOpen` Event for generation of random loot/treasure)

