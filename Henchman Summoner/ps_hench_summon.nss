//::///////////////////////////////////////////////
//:: ps_hench_summon
//:://////////////////////////////////////////////
/*
    Summon all possible companions to my location.
*/
//:://////////////////////////////////////////////
//:: Created By:  Palswim
//:: Created On:  2018-12-21
//:://////////////////////////////////////////////

#include "x0_i0_henchman"
#include "ps_hench_manip"

int SummonHench(object oHench, object obj) {
    object oMaster = GetMaster(oHench);
    if (GetIsObjectValid(oMaster) && (oMaster != obj)) {
        Log(1, (GetName(oHench) + " with Master: " + GetName(oMaster)));
        return FALSE;
    }
    AssignCommand(oHench, ActionJumpToObject(obj));
    Log(1, "Teleporting " + GetName(oHench));
    return TRUE;
}
int SummonHenchToObj(string tagHench, object obj) {
    Log(2, "Summoning Henchman with tag: " + tagHench);
    object oHench = GetObjectByTag(tagHench);
    if (GetIsObjectValid(oHench)) {
        if (GetPlayerHasHired(obj, oHench))
            return SummonHench(oHench, obj);
        else
            Log(1, (GetName(oHench) + " exists, but PC has not hired"));
    }
    return FALSE;
}
int SummonHenchToObjVar(string tagHench, object obj, string varToCheck = "hired") {
    object oHench = GetObjectByTag(tagHench);
    if (GetIsObjectValid(oHench)) {
        if (GetLocalInt(oHench, varToCheck))
            return SummonHench(oHench, obj);
        else
            Log(1, (GetName(oHench) + " has 0 value for : " + varToCheck));
    }
    return FALSE;
}

void SummonVarToObj(object obj, object oVar) {
    if (!GetIsObjectValid(oVar))
        oVar = obj;

    string henches = GetLocalString(oVar, NAME_VAR);
    Log(2, "Previous Henchmen: " + henches);
    int modHenches = FALSE;

    object cHench = GetHenchman(obj, 1); // If not a PC, then we'll have to parse by var string only
    int n = 2;
    while (GetIsObjectValid(cHench)) {
        string tagHench = GetTag(cHench);
        if (!HasMatchCSV(henches, DELIM_VAR, tagHench)) {
            henches = henches + DELIM_VAR + tagHench;
            Log(2, "Adding Henchman to string: " + tagHench);
            modHenches = TRUE;
            SetPlayerHasHired(obj, cHench);
            Log(0, "Remembering to teleport " + GetName(cHench));
        }
        cHench = GetHenchman(obj, n++);
    }

    int lenHenches = GetStringLength(henches);
    int posStart = 0;
    do {
        int posEnd = FindSubString(henches, DELIM_VAR, posStart);
        if (posEnd == -1)
            posEnd = lenHenches;
        if (posStart < posEnd)
            SummonHenchToObj(GetSubString(henches, posStart, (posEnd - posStart)), obj);
        posStart = posEnd + 1;
    } while (posStart < lenHenches);

    if (modHenches) {
        Log(2, "Setting new Henchmen value: " + henches);
        SetLocalString(oVar, NAME_VAR, henches);
    }
}

void SummonAllToObj(object obj) { // Old, static way
    // NW OC Companions
    SummonHenchToObj("NW_HEN_GRI", obj);
    SummonHenchToObj("NW_HEN_LIN", obj);
    SummonHenchToObj("NW_HEN_DAE", obj);
    SummonHenchToObj("NW_HEN_BOD", obj);
    SummonHenchToObj("NW_HEN_SHA", obj);
    SummonHenchToObj("NW_HEN_GAL", obj); // Tomi

    // NW SotU Companions
    SummonHenchToObj("x0_hen_dor", obj);
    SummonHenchToObj("x0_hen_xan", obj);
    SummonHenchToObj("x0_hen_dee", obj);

    // NW HotU Companions
    SummonHenchToObj("x2_hen_deekin", obj);
    SummonHenchToObj("x2_hen_nathyra", obj);
    SummonHenchToObj("x2_hen_valen", obj);
    SummonHenchToObj("H2_Aribeth", obj);
    SummonHenchToObj("x2_hen_valshar", obj);

    // The Aielund Saga companions
    SummonHenchToObjVar("NELLISE", obj);
    SummonHenchToObjVar("DANTE", obj);
    SummonHenchToObjVar("criosa", obj, "criosa"); // Strange Variable in Act 1
    SummonHenchToObjVar("CRIOSA", obj);
    SummonHenchToObjVar("RONAN", obj);
    SummonHenchToObjVar("VERA", obj);
    SummonHenchToObjVar("MAGGIE", obj);
    SummonHenchToObjVar("WILLIAM", obj);
    SummonHenchToObjVar("BLACK", obj);
    SummonHenchToObjVar("SPARTAN", obj, "sparhired");
    SummonHenchToObjVar("MAGGIE2", obj);
    SummonHenchToObj("CRIOSA", obj);                 // Strange Variables for Act 4, Part 3
    SummonHenchToObjVar("TERINUS", obj);
    SummonHenchToObjVar("JEZELLE", obj);
}

void main() {
    object obj = (GetIsObjectValid(GetItemActivated()) ? GetItemActivatedTarget() : OBJECT_SELF);
    SummonVarToObj(obj, GetModule());
}

