//::///////////////////////////////////////////////
//:: ps_hench_summon
//:://////////////////////////////////////////////
/*
    Summon, then join or release all companions.
*/
//:://////////////////////////////////////////////
//:: Created By:  Palswim
//:: Created On:  2020-06-26
//:://////////////////////////////////////////////

#include "ps_hench_manip"

void JoinHenchToMaster(object cMaster, string tagHench) {
    AddHenchman(cMaster, GetObjectByTag(tagHench));
}
void JoinVarToObj(object cPC, object oVar) {
    if (!GetIsObjectValid(oVar))
        oVar = cPC;

    string henches = GetLocalString(oVar, NAME_VAR);
    int lenHenches = GetStringLength(henches);
    int posStart = 0;
    do {
        int posEnd = FindSubString(henches, DELIM_VAR, posStart);
        if (posEnd == -1)
            posEnd = lenHenches;
        if (posStart < posEnd)
            JoinHenchToMaster(cPC, GetSubString(henches, posStart, (posEnd - posStart)));
        posStart = posEnd + 1;
    } while (posStart < lenHenches);
}

void RemoveHenchmanSafe(object cMaster, object cHench) {
    AssignCommand(cHench, ClearAllActions(TRUE));
    RemoveHenchman(cMaster, cHench);
}

void main() {
    ExecuteScript("ps_hench_summon", OBJECT_SELF);
    object cSelf = (GetIsObjectValid(GetItemActivated()) ? GetItemActivator() : OBJECT_SELF);
    int noHenchmen = TRUE;
    object cHench = GetHenchman(cSelf);
    while (GetIsObjectValid(cHench)) {
        noHenchmen = FALSE;
        RemoveHenchmanSafe(cSelf, cHench);
        cHench = GetHenchman(cSelf);
    }
    if (noHenchmen) // If we had no Henchmen in the first place
        JoinVarToObj(cSelf, GetModule());
}

