# Henchman Summoner

The **Henchman Summoner** allows a PC to summon his henchmen to his side from wherever he last left them.

## Background

There are many reasons to have a henchman by your side or to dismiss a henchman. But, in dismissing a henchman, too many times I forgot exactly where I left him, and so rather than hunting for my henchman throughout a module, I developed the Henchman Summoner to bring him to my side.

## Technical details

The player must use the Henchman Summoner with the henchmen in the party that he would like to summon later, once for each module. This will "register" them (add them to a variable) which the script will reference in subsequent summoning attempts. If a new henchman joins the party later, the player must use the Henchman Summoner again with the new henchman in the party before the Summoner will summon that henchman outside of the party. Adding a new henchman in this fashion does not require using the Henchman Summoner with all henchmen in the party, just the new one. To use this item as a player, place these files in your `override` directory, and ensure you are using [Tag-Based Scripting](https://forum.neverwintervault.org/t/adding-tag-based-scripting-to-oc-modules/1236) in your module. Of course, this will not work well with mutliple PCs that can hire the same henchmen. Again, on the transition to a new module, the player must re-register each henchman with the Henchman Summoner.

The Henchman Lasso works similarly to the Henchman Summoner, but in addition to summoning the henchmen, it also directly adds or dismisses them. It works as a toggle, in that if the PC has any henchmen, it will dismiss all of them. But, if the PC has no henchmen, it will hire (add) all of the henchmen from the variable after summoning them with the same mechanism as the Henchman Summoner.

Invocation of the script directly (`dm_runscript ps_hench_summon`) will summon the henchmen to the PC, as will usage of the item (`ps_it_summoner`) by the PC. If the item allowed a target other than Self, the script would summon the henchmen to the target location.

Unfortunately, different modules have different flags to describe the state of the player's henchmen. Earlier versions of the script tried to summon the henchmen statically, trying a set list of tags, but this list only covered the OCs and [the Aielund Saga](https://neverwintervault.org/project/nwn1/module/aielund-saga-act-i-nature-abhors-vacuum), and even then the differences in states between the modules was making the code ugly.

### Reasons to use Henchmen
 - Combat assistance
 - Larger encounters
 - Additional dialogue options
 - Enjoyment of banter

### Reasons to not use Henchmen
 - XP penalty
 - NPC reactions
 - Annoyance at banter
 - Equipment costs

## Files
 - `ps_it_summoner`: The Henchman Summoner (Miscellaneous Small item that can target self or a location to which to summon the henchmen)
 - `ps_it_summonjoin`: The Henchman Lasso (Miscellaneous Small item that can target self to either summon and add or dismiss the henchmen)
 - `ps_hench_manip`: The script file containing code common to both other scripts
 - `ps_hench_summon`: The script which the Henchman Summoner uses, but also works for the PC by invocation with `dm_runscript`
 - `ps_hench_join_t`: The script which the Henchman Lasso uses, but also works for the PC by invocation with `dm_runscript`

