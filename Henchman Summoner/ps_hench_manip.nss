//::///////////////////////////////////////////////
//:: ps_hench_manip
//:://////////////////////////////////////////////
/*
    Utilities to faciliate the manipulation of henchmen.
    Uses a delimited Module Variable to track which
    henchmen to manipulate.
*/
//:://////////////////////////////////////////////
//:: Created By:  Palswim
//:: Created On:  2020-02-26
//:://////////////////////////////////////////////

const int LOGLEVEL = 0;
const string NAME_VAR = "PS Henchmen";
const string DELIM_VAR = "|";

void Log(int level, string message) {
    if (level <= LOGLEVEL)
        SendMessageToPC(GetFirstPC(), message);
}

int HasMatchCSV(string source, string delim, string match) {
    int pos = FindSubString(source, match, 0);
    while (pos > -1) {
        int lenDelim = GetStringLength(delim);
        int lenSource = GetStringLength(source);
        int lenMatch = GetStringLength(match);
        Log(3, "Found match at " + IntToString(pos) + " for: " + match);
        if (
            (
                (pos == 0) || // If at beginning
                (GetSubString(source, (pos - lenDelim), lenDelim) == delim) // OR the delimiter precedes it
            )
            &&
            (
                (pos == (lenSource - lenMatch)) || // If at the end
                (GetSubString(source, (pos + lenMatch), lenDelim) == delim) // OR the delimiter follows it
            )
        )
            return TRUE;
        pos = FindSubString(source, match, pos + 1);
    }
    return FALSE;
}

