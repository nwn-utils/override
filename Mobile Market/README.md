# Mobile Market

The **Mobile Market** provides a store to open on demand from any location.

## Background

I am always considering ways to accomplish routine tasks faster, and from my very first play through the Neverwinter Nights OC, especially the Creator Ruins in Chapter 2, I have never enjoyed dragging loot at a reduced speed until finding an acceptable merchant. I suppose I could just leave the items, but instead of that, I developed the Mobile Market to allow for the opening of a store even in the remotest parts of the campaign.

## Technical Details

The Mobile Market uses a variable to determine the Store to open. To set this variable, use the Mobile Market on the in-game merchant NPC who triggers the Store you would like to use with the Mobile Market. Direct script invocation (`dm_runscript ps_store_general`) will also use the Store you have previously selected; without a previous selection, it will attempt to open a Store from a static list, but from [the Aielund Saga](https://neverwintervault.org/project/nwn1/module/aielund-saga-act-i-nature-abhors-vacuum) only. To use this item as a player, place these files in your `override` directory, and ensure you are using [Tag-Based Scripting](https://forum.neverwintervault.org/t/adding-tag-based-scripting-to-oc-modules/1236) in your module.

The Mobile Market uses a modified `gplotAppraiseOpenStore` call, which remembers the highest [Appraise](https://nwn.fandom.com/wiki/Appraise) skill, but does not include [the random variance that the OC used](https://www.gog.com/forum/neverwinter_nights_series/nwn1_appraise).

## Files
 - `ps_it_market`: The Mobile Market (Miscellaneous Small item that targets self to open the Store)
 - `ps_store_general`: The script which the Item uses, but also works for the PC by invocation with `dm_runscript`

