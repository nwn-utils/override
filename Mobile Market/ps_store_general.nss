//::///////////////////////////////////////////////
//:: FileName ps_store_general
//:://////////////////////////////////////////////
//:://////////////////////////////////////////////
//:: Created By: Pswim
//:: Created On: 2019-03-21
//:://////////////////////////////////////////////
#include "nw_i0_plot"
#include "x2_inc_switches"

const int LOGLEVEL = 0;
const string NAME_VAR = "PS Store";

int Log(int level, int ret, string message) {
    if (level <= LOGLEVEL)
        SendMessageToPC(GetFirstPC(), message);
    return ret;
}

void OpenTargetStore(object oPC, object oTarget, string tagStore) {
    if (oPC != oTarget) {
        object oStore = GetNearestObjectToLocation(OBJECT_TYPE_STORE, GetLocation(oTarget));
        string valSet = GetTag(oTarget) + "|" + GetTag(oStore);
        SetLocalString(oPC, tagStore, valSet);
    }
    else {
        string val = GetLocalString(oPC, tagStore);
        int pos = FindSubString(val, "|");
        if (pos < 1) {
            SendMessageToPC(oPC, "Could not parse value (" + tagStore + "): " + val);
            return;
        }
        string tagTarget = GetSubString(val, 0, pos);
        oTarget = GetObjectByTag(tagTarget);
        if (!GetIsObjectValid(oTarget)) {
            SendMessageToPC(oPC, "Invalid Merchant: " + tagTarget);
            return;
        }
    }
    if (LOGLEVEL)
        SendMessageToPC(oPC, "Targeting Merchant: " + GetName(oTarget));
    ExecuteScript("ps_store_general", oTarget);
}

void OpenStoreWrapper(object oPC, object oStore, int adjustment = 0) {
    gplotAppraiseOpenStore(oStore, oPC, (-adjustment), adjustment);
    if (LOGLEVEL) {
        string tagVar = ObjectToString(OBJECT_SELF);
        SendMessageToPC(oPC, GetName(OBJECT_SELF) + " opened store (" + tagVar + ") at adjustment: " + IntToString(-1 * GetLocalInt(oPC, "X0_APPRAISEADJUST" + tagVar)));
    }
}

int OpenStoreByTag(object oPC, string tagStore, int adjustment = 0) {
    object oStore = GetObjectByTag(tagStore);
    if (GetObjectType(oStore) != OBJECT_TYPE_STORE)
        return FALSE;
    AssignCommand(oStore, OpenStoreWrapper(oPC, oStore, adjustment));
    return TRUE;
}

void OpenPCStore_Static(object oPC) {
    if (LOGLEVEL)
        SendMessageToPC(oPC, "Locating store to open");

    int opened = OpenStoreByTag(oPC, "")
        || OpenStoreByTag(oPC, "")
        || OpenStoreByTag(oPC, "celeste")           // Aielund 1
        || OpenStoreByTag(oPC, "thad")              // Aielund 2
        || OpenStoreByTag(oPC, "ACADIA_SMITH")      // Aielund 3
        || OpenStoreByTag(oPC, "MYR_ARCHERY")       // Aielund 4.1
        || OpenStoreByTag(oPC, "quartermaster")     // Aielund 4.2
        || OpenStoreByTag(oPC, "TWILIGHT_MERCHANT") // Aielund 4.3
        ;
    if (!opened)
        SendMessageToPC(oPC, "Failed to open store");
}

int OpenStoreFromMerchant(object cPC, object cMerchant) {
    if (!GetIsObjectValid(cMerchant))
        return FALSE;
    object oStore = ((GetObjectType(cMerchant) == OBJECT_TYPE_STORE)
        ? cMerchant // Though we are expecting a Creature, a direct Store object will work
        : GetNearestObject(OBJECT_TYPE_STORE, cMerchant));
    if (!GetIsObjectValid(oStore))
        return FALSE;
    AssignCommand(cMerchant, OpenStoreWrapper(cPC, oStore));
    return TRUE;
}

int OpenStoreFromMerchantNew(object cPC, object cMerchant) {
    int success = OpenStoreFromMerchant(cPC, cMerchant);
    if (success)
        SetLocalString(cPC, NAME_VAR, GetTag(cMerchant));
    return success;
}

int OpenStoreFromVar(object cPC) {
    string tag = GetLocalString(cPC, NAME_VAR);
    object obj;
    int n = 0;
    do { // Support multiple objects with the same Tag
        object cMerchant = GetObjectByTag(tag, n++);
        if (!GetIsObjectValid(cMerchant)) {
            break;
        }
        else if (GetObjectType(cMerchant) == OBJECT_TYPE_CREATURE) { // Prefer an NPC
            if (OpenStoreFromMerchant(cPC, cMerchant))
                return TRUE;
        }
        else {
            obj = cMerchant;
        }
    } while (TRUE);

    return OpenStoreFromMerchant(cPC, obj) // If we didn't find an NPC, try with the last valid object
        || ((LOGLEVEL > 0)
            ? Log(1, FALSE, "Failed to open store for Tag: " + tag)
            : Log(0, FALSE, "Failed to open store"));
}

void main()
{
    int evtnum = GetUserDefinedItemEventNumber();
    if ((evtnum > 0) && (evtnum != X2_ITEM_EVENT_ACTIVATE))
        return;

    object oItem = GetItemActivated();
    if (GetIsObjectValid(oItem)) {
        object cPC = GetItemActivator();
        object oTarget = GetItemActivatedTarget();
        if ((cPC == oTarget) || (!OpenStoreFromMerchantNew(cPC, oTarget))) // Also fallback to Var if Target fails
            OpenStoreFromVar(cPC);
    } else {
        object cPC = GetIsPC(OBJECT_SELF) ? OBJECT_SELF : GetFirstPC();
        if (!OpenStoreFromVar(cPC))
            OpenPCStore_Static(cPC); // Fallback to old static list
    }
}
