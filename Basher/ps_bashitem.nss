//:://////////////////////////////////////////////
//:: Created By:  Palswim
//:: Created On:  2018-09-17
//:: Last Update: 2019-06-25
//:: Description: Bash all items in a radius around the Target.
//::   Useful for item cleanup and looting after combat/questing.
//::   Use outside of familiar, controlled environments can
//::   break a game (destroy a container that a module forgot to
//::   mark as indestructible)
//::   or achieve objectives too easily (e.g. in modules where
//::   the party must destroy a heavily-fortified door in-combat).
//:://////////////////////////////////////////////

//#include "x2_inc_switches" /// No longer using User-Defined Events

int DEBUG = FALSE;

void DestroyPlaceables(location lTarget);
void ReplinishAmmo(object oItem);

void LO(string name, object obj) {
    SendMessageToPC(GetFirstPC(), (name + ": " + (GetIsObjectValid(obj) ? GetName(obj) : "[INVALID]") + " (" + GetTag(obj) + ")"));
}

void LogObjs() {
    LO("OBJECT_SELF", OBJECT_SELF);
    LO("GetSpellCastItem", GetSpellCastItem());
    LO("GetSpellTargetObject", GetSpellTargetObject());
    LO("GetLastSpellCaster", GetLastSpellCaster());
    LO("GetItemActivated", GetItemActivated());
    LO("GetItemActivatedTarget", GetItemActivatedTarget());
    LO("GetItemActivator", GetItemActivator());
}

void main() {
    if (DEBUG)
        LogObjs();
    object cActor = OBJECT_SELF;
    object item = OBJECT_INVALID;
    object oTarget = OBJECT_SELF;
    location lTarget;

    if (GetIsObjectValid(item = GetItemActivated())) { // Item Activation
        cActor = GetItemActivator();
        lTarget = (GetIsObjectValid(oTarget = GetItemActivatedTarget()) ? GetLocation(oTarget) : GetItemActivatedTargetLocation());
    }
    else {                                             // Direct invocation or Casting
        item = GetSpellCastItem(); // Not entirely reliable; could return the last Item if the current invocation doesn't involve an Item
        cActor = ((GetObjectType(OBJECT_SELF) == OBJECT_TYPE_CREATURE) ? OBJECT_SELF : GetLastSpellCaster());
        if (GetIsObjectValid(oTarget = GetSpellTargetObject()))
            lTarget = GetLocation(oTarget);
        else if (GetIsObjectValid(item))
            lTarget = GetSpellTargetLocation();
        else
            lTarget = GetLocation(oTarget = cActor);
    }

    if (DEBUG)
        SendMessageToPC((GetIsPC(cActor) ? cActor : GetFirstPC()), ("Actor: " + GetName(cActor) + ", Item: " + GetName(item) + ", Target: " + GetName(oTarget)));

    AssignCommand(cActor, DestroyPlaceables(lTarget));
    if (GetIsObjectValid(item))
        ReplinishAmmo(item);
}

/*
void main_UDEvent()
{
    object oPC;
    object oItem;
    object oTarget;

    //SendMessageToPC(GetFirstPC(),IntToString(nEvent)); // DEBUG/Logging

    switch (GetUserDefinedItemEventNumber()) {
    // * This code runs when the item has the OnHitCastSpell: Unique power property
    // * and it hits a target(weapon) or is being hit (armor)
    // * Note that this event fires for non PC creatures as well.
    case X2_ITEM_EVENT_ONHITCAST:
        oItem = GetSpellCastItem();          // The item casting triggering this spellscript
        //object oSpellOrigin = OBJECT_SELF;
        oTarget = GetSpellTargetObject();
        //oPC = OBJECT_SELF;
        DestroyPlaceables(oTarget);
        ReplinishAmmo(oItem);
        break;
    // * This code runs when the Unique Power property of the item is used
    // * Note that this event fires PCs only
    case X2_ITEM_EVENT_ACTIVATE:
        oTarget = GetItemActivatedTarget();
        oPC = GetItemActivator();
        if (oPC == oTarget)
            ExecuteScript("ps_bashitem", oPC);
        else
            DestroyPlaceables(oTarget);
        break;
    // * This code runs when the item is equipped
    // * Note that this event fires PCs only
    case X2_ITEM_EVENT_EQUIP:
        oPC   = GetPCItemLastEquippedBy();
        oItem = GetPCItemLastEquipped();
        break;
    // * This code runs when the item is unequipped
    // * Note that this event fires PCs only
    case X2_ITEM_EVENT_UNEQUIP:
        oPC    = GetPCItemLastUnequippedBy();
        oItem  = GetPCItemLastUnequipped();
        break;
    // * This code runs when the item is acquired
    // * Note that this event fires PCs only
    case X2_ITEM_EVENT_ACQUIRE:
        oPC    = GetModuleItemAcquiredBy();
        oItem  = GetModuleItemAcquired();
        break;
    // * This code runs when the item is unaquire d
    // * Note that this event fires PCs only
    case X2_ITEM_EVENT_UNACQUIRE:
        oPC    = GetModuleItemLostBy();
        oItem  = GetModuleItemLost();
        break;
    //* This code runs when a PC or DM casts a spell from one of the
    //* standard spellbooks on the item
    case X2_ITEM_EVENT_SPELLCAST_AT:
        oPC    = GetLastSpellCaster();
        oItem  = GetSpellTargetObject();
        break;
    default:
        DestroyPlaceables(OBJECT_SELF);
        break;
    }
}
*/

void DestroyPlaceables_Obj(object oTarget) {
    location lTarget;
    if(GetIsObjectValid(oTarget))
        lTarget = GetLocation(oTarget);
    else
        lTarget = GetSpellTargetLocation();

    DestroyPlaceables(lTarget);
}
void DestroyPlaceables(location lTarget) {
    effect eVisFB = EffectVisualEffect(VFX_FNF_FIREBALL);
    ApplyEffectAtLocation(DURATION_TYPE_INSTANT, eVisFB, lTarget);

    // Find all (Placeable) Objects within a 40-meter cube (20-meter "radius") and explode them
    object obj = GetFirstObjectInShape(SHAPE_CUBE, 20.0, lTarget, FALSE, (OBJECT_TYPE_PLACEABLE | OBJECT_TYPE_DOOR));
    while(GetIsObjectValid(obj)) {
        effect eVis = EffectVisualEffect(VFX_DUR_ELEMENTAL_SHIELD);
        ApplyEffectToObject(DURATION_TYPE_INSTANT, eVis, obj);
        effect eDmg = EffectDamage(999);
        ApplyEffectToObject(DURATION_TYPE_INSTANT, eDmg, obj);
        obj = GetNextObjectInShape(SHAPE_CUBE, 20.0, lTarget, FALSE, (OBJECT_TYPE_PLACEABLE | OBJECT_TYPE_DOOR));
    }
}

void ReplinishAmmo(object oItem) {
    int typeItem = GetBaseItemType(oItem);
    if ((typeItem == BASE_ITEM_ARROW) || (typeItem == BASE_ITEM_BOLT) || (typeItem == BASE_ITEM_BULLET)) {
        SetItemStackSize(oItem, 99); // Replenish Ammo
    }
}
