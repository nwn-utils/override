# The Basher

**The Basher** bashes the surrounding objects, destroying all non-indestructible objects.

## Background

In multi-Module campaigns, I like to sweep through the Module after I finish it, picking up any loot that I may have left as I was working my way through the Quests. To that end, I also like to ensure that I don't waste time checking the same container, so I developed some "Bashing" items for this phase of the campaign.

## Technical details

Upon use of the item or hit by one of these items, the code will destroy (rather, deal 999 magical damage to) all Placeables and Doors within a 40m cube of the target item/location. Direct invocation of the script (`dm_runscript ps_bashitem`) will center this destruction around the PC. To use these items as a player, extract the contents to your `override` directory, and then add or spawn items (`dm_spawnitem ps_it_basher`) to your heart's content. These items require [Tag-Based Scripting](https://forum.neverwintervault.org/t/adding-tag-based-scripting-to-oc-modules/1236) to work in your module.

## Files
 - `ps_it_basharw`: Bashing Arrow
 - `ps_it_bashbo`: Bolt of Bashing
 - `ps_it_bashbu`: Bullet of Bashing
 - `ps_it_basher`: The Basher (Miscellaneous Small item that bashes on use)
 - `ps_it_bashring`: Ring of Bashing
 - `ps_it_bashringsp`: Ring of Speed and Bashing (adds Haste)
 - `ps_bashitem`: The script which the Item uses, but also works for the PC by invocation with `dm_runscript`
 - `ps_bashloot`: Script which combines the features of The Basher with [The Looter](../Looter), but does everything in one command, rather than having to follow usage of The Basher with usage of The Looter. Requires [`ps_lootitems` from The Looter](../Looter/ps_lootitems.ncs).
